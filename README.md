http://antoine-prudhomme.ch/preparateur-mental/
Préparateur mental - Antoine Prud'homme
Antoine Prud'homme
077 470 45 36
prudhomme.antoine@orange.fr
Suisse
Préparateur mental : La préparation mentale vise l’entrainement des habiletés mentales, après les avoir évalué. Le préparateur mental travaille sur la capacité individuelle et collective à mieux gérer son stress, ses émotions, sa con?ance en soi, sa concentration, sa relaxation, son imagination.